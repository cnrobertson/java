package com.milapy.parser.dao;

import javax.persistence.EntityManager;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class ItemDAOReadTest
{
	@BeforeClass
	public static void setUp ( )
	{
	  em = PersistenceManager.INSTANCE.getEntityManager();
	}
	
	@AfterClass
	public static void tearDown ( )
	{
	  //em.close();
	  //PersistenceManager.INSTANCE.close();
	}
	@Test
	public void queryShouldReturnItem ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    Assert.assertNotNull("Item not found", item);
	}

	@Test
	public void queryShouldReturnCorrectTitle ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    String expectedTitle = "USOC Creates Panel to Address Athletes Zika Concerns";
	    Assert.assertNotNull("Item not found", item);
	    Assert.assertEquals("Incorrect title", expectedTitle, item.getTitle());
	}
	
	@Test
	public void queryShouldReturnCorrectContent ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    String expectedContent = "U.S. athletes hoping to make the trip to Rio for the 2016 Summer Olympics are concerned over growing Zika virus cases.show enclosure(image/jpeg)";
	    Assert.assertNotNull("Item not found", item);
	    Assert.assertEquals("Incorrect content", expectedContent, item.getContent());
	}
	
	@Test
	public void queryShouldReturnCorrectLocation ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    String expectedTitle = "Rio";
	    Assert.assertNotNull("Item not found", item);
	    Assert.assertEquals("Incorrect title", expectedTitle, item.getParserLocation());
	}
	
	@Test
	public void queryShouldReturnCorrectOrganization ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    String expectedOrganization = "USOC Creates Panel";
	    Assert.assertNotNull("Item not found", item);
	    Assert.assertEquals("Incorrect organization", expectedOrganization, item.getParserOrganization());
	}
	
	@Test
	public void queryShouldReturnCorrectPerson ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    String expectedPerson = "Zika";
	    Assert.assertNotNull("Item not found", item);
	    Assert.assertEquals("Incorrect person", expectedPerson, item.getParserPerson());
	}
	
	private static EntityManager em; 
}
