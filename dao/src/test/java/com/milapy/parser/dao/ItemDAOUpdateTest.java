package com.milapy.parser.dao;

import javax.persistence.EntityManager;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

public class ItemDAOUpdateTest
{
	@BeforeClass
	public static void setUp ( )
	{
	  em = PersistenceManager.INSTANCE.getEntityManager();
	}
	
	@AfterClass
	public static void tearDown ( )
	{
	  //em.close();
	  //PersistenceManager.INSTANCE.close();
	}

	@Test
	public void queryShouldReturnItem ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    Assert.assertNotNull("Item not found", item);
	}

	@Test
	public void queryThenUpdateShouldReturnNewTitle ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    String expectedTitle = "USOC Creates Panel to Address Athletes Zika Concerns";
	    Assert.assertNotNull("Item not found", item);
	    Assert.assertEquals("Incorrect title", expectedTitle, item.getTitle());

	    String newTitle = "Updated - USOC Creates Panel to Address Athletes Zika Concerns";
	    em.getTransaction()
        .begin();
	    item.setTitle(newTitle);
	    em.getTransaction().commit();
	    
	    item = em.find(ItemDAO.class, 1);
	    Assert.assertEquals("title not updated", newTitle, item.getTitle());


	    em.getTransaction()
        .begin();
	    item.setTitle(expectedTitle);
	    em.getTransaction().commit();
	    
	    item = em.find(ItemDAO.class, 1);
	    Assert.assertEquals("title not reverted", expectedTitle, item.getTitle());
	}
	
	@Test
	public void queryThenUpdateShouldReturnNewLocation ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    String expectedLocation = "Rio";
	    Assert.assertNotNull("Item not found", item);
	    Assert.assertEquals("Incorrect location", expectedLocation, item.getParserLocation());

	    String newLocation = "Updated - Glasgow";
	    em.getTransaction()
        .begin();
	    item.setParserLocation(newLocation);
	    em.getTransaction().commit();
	    
	    item = em.find(ItemDAO.class, 1);
	    Assert.assertEquals("location not updated", newLocation, item.getParserLocation());

	    em.getTransaction()
        .begin();
	    item.setParserLocation(expectedLocation);
	    em.getTransaction().commit();
	    
	    item = em.find(ItemDAO.class, 1);
	    Assert.assertEquals("location not reverted", expectedLocation, item.getParserLocation());
	}

	@Test
	public void queryThenUpdateShouldReturnNewOrganization ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    String expectedOrganization = "USOC Creates Panel";
	    Assert.assertNotNull("Item not found", item);
	    Assert.assertEquals("Incorrect organization", expectedOrganization, item.getParserOrganization());

	    String newOrganization = "Updated - USOC Creates Panel";
	    em.getTransaction()
        .begin();
	    item.setParserOrganization(newOrganization);
	    em.getTransaction().commit();
	    
	    item = em.find(ItemDAO.class, 1);
	    Assert.assertEquals("organization not updated", newOrganization, item.getParserOrganization());

	    em.getTransaction()
        .begin();
	    item.setParserOrganization(expectedOrganization);
	    em.getTransaction().commit();
	    
	    item = em.find(ItemDAO.class, 1);
	    Assert.assertEquals("organization not reverted", expectedOrganization, item.getParserOrganization());
	}
	
	@Test
	public void queryThenUpdateShouldReturnNewPerson ( )
	{
	    ItemDAO item = em.find(ItemDAO.class, 1);
	    
	    String expectedPerson = "Zika";
	    Assert.assertNotNull("Item not found", item);
	    Assert.assertEquals("Incorrect person", expectedPerson, item.getParserPerson());

	    String newPerson = "Updated - Zika";
	    em.getTransaction()
        .begin();
	    item.setParserPerson(newPerson);
	    em.getTransaction().commit();
	    
	    item = em.find(ItemDAO.class, 1);
	    Assert.assertEquals("person not updated", newPerson, item.getParserPerson());

	    em.getTransaction()
        .begin();
	    item.setParserPerson(expectedPerson);
	    em.getTransaction().commit();
	    
	    item = em.find(ItemDAO.class, 1);
	    Assert.assertEquals("person not reverted", expectedPerson, item.getParserPerson());
	}
	
	private static EntityManager em; 
}
