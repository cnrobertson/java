package com.milapy.parser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ProperNoun {



  public ProperNoun ( List<String> aListOfElements )
  {
	this.elements = new ArrayList<String>(aListOfElements);
  }

  public List<String> getElements ()
  {
	  return (elements);
  }
  
  public String toString ( )
  {
	  String result = "";
	  
	  Iterator<String> iterator = elements.iterator();
	  while (iterator.hasNext())
	  {
        if (result.length() > 0)
        {
        	result+=" ";
        }
        result += iterator.next();
	  }
	  
	  return (result);
  }
  private List<String> elements;
}
