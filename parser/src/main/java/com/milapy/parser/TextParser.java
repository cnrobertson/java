package com.milapy.parser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.Tree;

public class TextParser {

	public TextParser ( )
	{
      String parserModel = "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz";
      parser = LexicalizedParser.loadModel(parserModel);		
	}
	
	public List<ProperNoun> parseSentence( String aSentence )
	{
	    TokenizerFactory<CoreLabel> tokenizerFactory =
	        PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
	    Tokenizer<CoreLabel> tok =
	        tokenizerFactory.getTokenizer(new StringReader(aSentence));
	    List<CoreLabel> rawWords = tok.tokenize();

	    Tree parse = parser.apply(rawWords);
	    parse.pennPrint();
	    System.out.println();
	    
	    Builder<Tree> builder = Stream.builder();
	    parse.forEach(element -> builder.add(element));
	    Stream<Tree> parserStream = builder.build();
	    
	    List<Tree> listOfNps = new ArrayList<Tree>();
	    parserStream.filter(t->t.label().value().equals("NP")).forEach(t -> listOfNps.add(t));
	    	    
	    List<ProperNoun> names = new ArrayList<ProperNoun>();
	    for (Tree np : listOfNps)
	    {
	      List<String> nameElements = new ArrayList<String>();
	      for (Tree child: np.children())
	      {
    	    if (child.label().value().equals("NNP"))
    	    {
    		  nameElements.add(child.children()[0].label().value());
    	    }
	      }
   	      if (nameElements.size() > 0)
  	      {
   	        ProperNoun newName = new ProperNoun(nameElements);
  		    names.add(newName);
  	      }
	    }
	    
	    names.stream().forEach(n->System.out.print(n.toString() + " "));
	    return (names);
	}
	
	private LexicalizedParser parser;
}
