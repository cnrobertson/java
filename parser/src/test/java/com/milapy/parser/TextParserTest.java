package com.milapy.parser;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Stream;
import java.util.stream.Stream.Builder;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;
import edu.stanford.nlp.trees.Tree;


public class TextParserTest {


	public List<ProperNoun> testParseSentence( String aSentence )
	{
	    TextParser classUnderTest = new TextParser();
	    
	    List<ProperNoun> result = classUnderTest.parseSentence(aSentence);
	    
	    return (result);
	}

	public List<ProperNoun> testParseArticle( String anArticle )
	{
	  List<ProperNoun> result = new ArrayList<ProperNoun>();
      StringTokenizer tokenizer = new StringTokenizer(anArticle, ".");
      
      List<String> sentences = new ArrayList<String>();
      while (tokenizer.hasMoreTokens())
      {
    	  sentences.add(tokenizer.nextToken() + ".");
      }
		    
	  for ( String sentence : sentences)
	  {
        List<ProperNoun> names = this.testParseSentence(sentence);
        
        for (ProperNoun name : names)
        {
        	result.add(name);
        }
	  }
	  
	  return (result);
	}

	public void testParseText ( String       aText,
			                    int          anExpectedNumberOfNames,
			                    List<String> anExpectedNameList )
	{
	    List<ProperNoun> result = this.testParseArticle(aText);
	    
	    Assert.assertEquals("Incorrect number of names returned", anExpectedNumberOfNames, result.size());
	    
	    int index = 0;
	    for ( ProperNoun name : result)
	    {
	      String expectedName = anExpectedNameList.get(index);
	      
	      if (expectedName != null)
	      {
	        this.assertNamesEqual(expectedName, name.getElements()	);
	      }
	      index++;
	    }
	}

	@Test
	public void testParseCharlotte()
	{
		List<String> expectedNames = new ArrayList<String>();
		expectedNames.add("Charlotte");
	    String testText = "This test was written in Charlotte.";
	    int expectedNumberOfNames = 1;
	    this.testParseText(testText, expectedNumberOfNames, expectedNames);
	}

	@Test
	public void testApplyGlasgow()
	{
		List<String> expectedNames = new ArrayList<String>();
		expectedNames.add("Glasgow");
	    String testText = "Glasgow's notorious Barras earmarked as arts events and music quarter in 30m plans.";
	    int expectedNumberOfNames = 1;
	    this.testParseText(testText, expectedNumberOfNames, expectedNames);
	}

	@Test
	public void testApplyGlasgowCommonWealthGames()
	{
		List<String> expectedNames = new ArrayList<String>();
	    int expectedNumberOfNames = 5;
		expectedNames.add("Glasgow");
		expectedNames.add(null);
		expectedNames.add("Commonwealth");
		expectedNames.add("Scotland");
		expectedNames.add(null);
		String testText = "Groundbreaking Glasgow 2014 legacy jobs scheme to be targeted in council cuts. A PIONEERING jobs scheme set up as part of the Commonwealth Games legacy will be a casualty as Scotland's largest council prepares to announce over Â£130million of cuts.";
	    this.testParseText(testText, expectedNumberOfNames, expectedNames);
	}

	@Test
	public void testApplyFukushimaTitle()
	{
		List<String> expectedNames = new ArrayList<String>();
	    int expectedNumberOfNames = 1;
		expectedNames.add("Fukushima");
		String testText = "10000 Fukushima children still live outside prefecture after disaster survey shows.";
	    this.testParseText(testText, expectedNumberOfNames, expectedNames);
	}

	@Test
	public void testApplyFukushimaContent1()
	{
		List<String> expectedNames = new ArrayList<String>();
	    int expectedNumberOfNames = 3;
		expectedNames.add("Fukushima Prefecture");
		expectedNames.add("March");
		expectedNames.add("Thursday");
		String testText = "Some 10,000 children whose families fled Fukushima Prefecture due to the March 2011 nuclear disaster have yet to return, prefectural government officials said Thursday.";
	    this.testParseText(testText, expectedNumberOfNames, expectedNames);
	}

	@Test
	public void testApplyFukushimaContent2()
	{
		List<String> expectedNames = new ArrayList<String>();
	    int expectedNumberOfNames = 1;
		expectedNames.add("Fukushima No");
		String testText = "Five years after the earthquake and ensuing tsunami triggered the radiation crisis at the Fukushima No. 1 nuclear complex, families with children continue to have serious reservations about environmental safety.";
	    this.testParseText(testText, expectedNumberOfNames, expectedNames);
	}

	@Test
	public void testApplyNarisawaTokyoTitle()
	{
		List<String> expectedNames = new ArrayList<String>();
	    int expectedNumberOfNames = 2;
		expectedNames.add("Narisawa");
		expectedNames.add("Tokyo");
		String testText = " Narisawa in Tokyo retains second spot in Asian restaurant rankings.";
	    this.testParseText(testText, expectedNumberOfNames, expectedNames);
	}

	@Test
	public void testApplyNarisawaTokyoContent1()
	{
		List<String> expectedNames = new ArrayList<String>();
	    int expectedNumberOfNames = 2;
		expectedNames.add("Tokyo");
		expectedNames.add("Asia");
		String testText = "French restaurant Narisawa in Tokyo retained the second spot on the list of the top 50 restaurants in Asia for the third consecutive year.";
	    this.testParseText(testText, expectedNumberOfNames, expectedNames);
	}

	@Test
	public void testApplyNarisawaTokyoContent2()
	{
		List<String> expectedNames = new ArrayList<String>();
	    int expectedNumberOfNames = 4;
		expectedNames.add("William Reed Business Media Ltd.");
		expectedNames.add(null);
		expectedNames.add("Monday");
		expectedNames.add("Bangkok");
		String testText = "William Reed Business Media Ltd., the organizer of the ranking, announced this year&#8217;s list Monday in Bangkok.";
	    this.testParseText(testText, expectedNumberOfNames, expectedNames);
	}

	@Test
	public void testApplyNarisawaTokyoContent3()
	{
		List<String> expectedNames = new ArrayList<String>();
	    int expectedNumberOfNames = 2;
		expectedNames.add("Indian Gaggan");
		expectedNames.add("Bangkok");
		String testText = "Indian restaurant Gaggan in Bangkok remained at the top for the second year in a row.";
	    this.testParseText(testText, expectedNumberOfNames, expectedNames);
	}

	private void assertNamesEqual( String       anExpectedName,
			                       List<String> aName )
	{
	  String name = String.join(" ", aName);
	  
	  Assert.assertEquals("Incorrect name returned", anExpectedName, name);
	}
}
