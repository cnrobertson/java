package com.milapy.classifier;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.milapy.parser.dao.ItemDAO;
import com.milapy.parser.dao.PersistenceManager;

public class NewsItemClassifierIT {

	@BeforeClass
	public static void setUp ( )
	{
	  em = PersistenceManager.INSTANCE.getEntityManager();
	}
	
//	@Test
//	public void testItemParser() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testParse() {
//		long dbStartTime = System.currentTimeMillis();
//		
//		ItemDAO daoItem1 = em.find(ItemDAO.class, 1);
//		ItemDAO daoItem2 = em.find(ItemDAO.class, 2);
//		ItemDAO daoItem3 = em.find(ItemDAO.class, 3);
//		ItemDAO daoItem4 = em.find(ItemDAO.class, 4);
//		ItemDAO daoItem5 = em.find(ItemDAO.class, 5);
//		ItemDAO daoItem6 = em.find(ItemDAO.class, 6);
//		ItemDAO daoItem7 = em.find(ItemDAO.class, 7);
//		ItemDAO daoItem8 = em.find(ItemDAO.class, 8);
//		ItemDAO daoItem9 = em.find(ItemDAO.class, 9);
//		ItemDAO daoItem10 = em.find(ItemDAO.class, 10);
//		List<ItemDAO> daoItems = new ArrayList<ItemDAO>();
//		daoItems.add(daoItem1);
//		daoItems.add(daoItem2);
//		daoItems.add(daoItem3);
//		daoItems.add(daoItem4);
//		daoItems.add(daoItem5);
//		daoItems.add(daoItem6);
//		daoItems.add(daoItem7);
//		daoItems.add(daoItem8);
//		daoItems.add(daoItem9);
//		daoItems.add(daoItem10);
//		
//		long dbEndTime = System.currentTimeMillis();
//		System.out.println("Retrieved 10 entries in " + (dbEndTime - dbStartTime) + " ms");
//		
//		long parseStartTime = System.currentTimeMillis();
//		List<NewsItem> items = new ArrayList<NewsItem>();
//		daoItems.forEach(i->items.add(new NewsItem(i)));
//		ItemParser classUnderTest = new ItemParser();
//		List<NewsItem> parsedItems = classUnderTest.parse(items);
//		long parseEndTime = System.currentTimeMillis();		
//		System.out.println("Parsed " + items.size() + " items in " + (parseEndTime - parseStartTime) + " ms");
//
//		Assert.assertEquals("Incorrect parsed list size ", 10, parsedItems.size());
//	}

	private void testClassifyItems( String aWhereClause )
	{
	  long dbStartTime = System.currentTimeMillis();
	  List<ItemDAO> daoItems = em.createQuery("Select t from " + ItemDAO.class.getSimpleName() + " t where " + aWhereClause).getResultList();

	  long dbEndTime = System.currentTimeMillis();
	  System.out.println("Retrieved " + daoItems.size() + " entries in " + (dbEndTime - dbStartTime) + " ms");
	
	  long startTime = System.currentTimeMillis();
	  List<NewsItem> items = new ArrayList<NewsItem>();
	  daoItems.forEach(i->items.add(new NewsItem(i)));
	  NewsItemClassifier classUnderTest = new NewsItemClassifier();
	  items.forEach(i->this.classifyAndUpdateItem(classUnderTest, i));
	
	  long endTime = System.currentTimeMillis();		
	  System.out.println("Processed " + items.size() + " items in " + (endTime - startTime) + " ms");

	  //Assert.assertEquals("Incorrect parsed list size ", items.size(), classifiedItems.size());
	}

  private NewsItem classifyItem ( NewsItemClassifier aClassifier,
                                  NewsItem           anItem )
  {
    List<NewsItem> items = new ArrayList<NewsItem>();
    items.add(anItem);
    List<NewsItem> classifiedItems = aClassifier.classify(items);
    NewsItem result = classifiedItems.get(0);
    return (result);   
  }
    
  private void classifyAndUpdateItem ( NewsItemClassifier aClassifier,
		                               NewsItem           anItem )
  {
    List<NewsItem> items = new ArrayList<NewsItem>();
    items.add(anItem);
    List<NewsItem> classifiedItems = aClassifier.classify(items);
    NewsItem classifiedItem = classifiedItems.get(0);
    
    EntityTransaction txn = em.getTransaction();
    
    txn.begin();
    em.merge(classifiedItem.getDao());
    txn.commit();    
  }
//	@Test
//	public void testClassify_France24Articles ( )
//	{
//	  this.testClassifyItems("feed_id =243");
//	}
//
//	@Test
//	public void testClassify_KuwaitTimesArticles ( )
//	{
//	  this.testClassifyItems("feed_id =244");
//	}
//
//	@Test
//	public void testClassify_XINHUANEWSArticles ( )
//	{
//	  this.testClassifyItems("feed_id =245");
//	}
//
//	@Test
//	public void testClassify_TheIndianExpressArticles ( )
//	{
//	  this.testClassifyItems("feed_id =246");
//	}
//
//	@Test
//	public void testClassify_DailyIndonesiaArticles ( )
//	{
//	  this.testClassifyItems("feed_id =251");
//	}
//
//	@Test
//	public void testClassify_DailySabahArticles ( )
//	{
//	  this.testClassifyItems("feed_id =255");
//	}
//
//	@Test
//	public void testClassify_TheChosunIlboArticles ( )
//	{
//	  this.testClassifyItems("feed_id =256");
//	}
//
//	@Test
//	public void testClassify_ThaiPBSEnglishNewsArticles ( )
//	{
//	  this.testClassifyItems("feed_id =257");
//	}
//
//	@Test
//	public void testClassify_News24SouthAfricaArticles ( )
//	{
//	  this.testClassifyItems("feed_id =261");
//	}
//
//	@Test
//	public void testClassify_GlasgoHeraldArticles ( )
//	{
//	  this.testClassifyItems("feed_id =263");
//	}
//
//	@Test
//	public void testClassify_DundeeCourierArticles ( )
//	{
//	  this.testClassifyItems("feed_id =270");
//	}
//
//	@Test
//	public void testClassify_DailyRecordArticles ( )
//	{
//	  this.testClassifyItems("feed_id =339");
//	}
//
//	@Test
//	public void testParse_Item3235_YemenPeaceTalks ( )
//	{
//	  this.testParseItems("id = 3235");
//	}
//	
//	@Test
//	public void testParse_Item5812_ACMAwardsLasVegas ( )
//	{
//	  this.testParseItems("id = 5812");
//	}

//	@Test
//	public void testParse_Item6193_HoustonFireDepartment ( )
//	{
//	  this.testParseItems("id = 6193");
//	}
//
//	@Test
//	public void testParse_Item5235_IndianSupremeCourtMaggi ( )
//	{
//	  this.testParseItems("id = 5235");
//	}
//	@Test
//	public void testParseItemContent() {
//		fail("Not yet implemented");
//	}
//	@Test
//	public void testClassify_AllPreviouslyClassifiedByPHPAlgorithm ( )
//	{
//	  this.testClassifyItems("item_vlocation='ALGO'");
//	}
//
//	@Test
//	public void testClassify_AllNewlyHarvestedItems ( )
//	{
//	  this.testClassifyItems("item_vlocation=''");
//	}
//
//	@Test
//	public void testParse_Item5235_ForestfiresInUttarakhand ( )
//	{
//	  this.testClassifyItems("id = 13995");
//	}

	@Test
	public void testClassifyItem ( )
	{
	  this.testClassifyItems("item_vlocation is null AND to_timestamp(item_published) > CURRENT_DATE - 3");
	  //this.testClassifyItems("item_vlocation=''");
	}

  

	private static EntityManager em;

}
