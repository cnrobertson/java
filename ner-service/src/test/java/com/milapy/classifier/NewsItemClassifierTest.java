package com.milapy.classifier;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.milapy.parser.dao.ItemDAO;
import com.milapy.parser.dao.PersistenceManager;

public class NewsItemClassifierTest {

	@BeforeClass
	public static void setUp ( )
	{
	}
	
//	@Test
//	public void testItemParser() {
//		fail("Not yet implemented");
//	}
//
//	@Test
//	public void testParse() {
//		long dbStartTime = System.currentTimeMillis();
//		
//		ItemDAO daoItem1 = em.find(ItemDAO.class, 1);
//		ItemDAO daoItem2 = em.find(ItemDAO.class, 2);
//		ItemDAO daoItem3 = em.find(ItemDAO.class, 3);
//		ItemDAO daoItem4 = em.find(ItemDAO.class, 4);
//		ItemDAO daoItem5 = em.find(ItemDAO.class, 5);
//		ItemDAO daoItem6 = em.find(ItemDAO.class, 6);
//		ItemDAO daoItem7 = em.find(ItemDAO.class, 7);
//		ItemDAO daoItem8 = em.find(ItemDAO.class, 8);
//		ItemDAO daoItem9 = em.find(ItemDAO.class, 9);
//		ItemDAO daoItem10 = em.find(ItemDAO.class, 10);
//		List<ItemDAO> daoItems = new ArrayList<ItemDAO>();
//		daoItems.add(daoItem1);
//		daoItems.add(daoItem2);
//		daoItems.add(daoItem3);
//		daoItems.add(daoItem4);
//		daoItems.add(daoItem5);
//		daoItems.add(daoItem6);
//		daoItems.add(daoItem7);
//		daoItems.add(daoItem8);
//		daoItems.add(daoItem9);
//		daoItems.add(daoItem10);
//		
//		long dbEndTime = System.currentTimeMillis();
//		System.out.println("Retrieved 10 entries in " + (dbEndTime - dbStartTime) + " ms");
//		
//		long parseStartTime = System.currentTimeMillis();
//		List<NewsItem> items = new ArrayList<NewsItem>();
//		daoItems.forEach(i->items.add(new NewsItem(i)));
//		ItemParser classUnderTest = new ItemParser();
//		List<NewsItem> parsedItems = classUnderTest.parse(items);
//		long parseEndTime = System.currentTimeMillis();		
//		System.out.println("Parsed " + items.size() + " items in " + (parseEndTime - parseStartTime) + " ms");
//
//		Assert.assertEquals("Incorrect parsed list size ", 10, parsedItems.size());
//	}

	private void testClassifyItems( String aWhereClause )
	{
	  long dbStartTime = System.currentTimeMillis();
	  List<ItemDAO> daoItems = getEntityManager().createQuery("Select t from " + ItemDAO.class.getSimpleName() + " t where " + aWhereClause).getResultList();

	  long dbEndTime = System.currentTimeMillis();
	  System.out.println("Retrieved " + daoItems.size() + " entries in " + (dbEndTime - dbStartTime) + " ms");
	
	  long startTime = System.currentTimeMillis();
	  List<NewsItem> items = new ArrayList<NewsItem>();
	  daoItems.forEach(i->items.add(new NewsItem(i)));
	  NewsItemClassifier classUnderTest = new NewsItemClassifier();
	  // items.forEach(i->this.classifyItem(classUnderTest, i));
	  List<NewsItem> classifiedItems = classUnderTest.classify(items);
	
	  long endTime = System.currentTimeMillis();		
	  System.out.println("Processed " + items.size() + " items in " + (endTime - startTime) + " ms");

//	  Assert.assertEquals("Incorrect parsed list size ", items.size(), classifiedItems.size());
//      EntityTransaction txn = getEntityManager().getTransaction();
// 
//      txn.begin();
//      classifiedItems.forEach(i->getEntityManager().merge(i.getDao()));
//      txn.commit();
     
	}

  private NewsItem classifyItem ( NewsItemClassifier aClassifier,
                                  NewsItem           anItem )
  {
    List<NewsItem> items = new ArrayList<NewsItem>();
    items.add(anItem);
    List<NewsItem> classifiedItems = aClassifier.classify(items);
    NewsItem result = classifiedItems.get(0);
    return (result);   
  }
    
  private void classifyAndUpdateItem ( NewsItemClassifier aClassifier,
		                               NewsItem           anItem )
  {
    List<NewsItem> items = new ArrayList<NewsItem>();
    items.add(anItem);
    List<NewsItem> classifiedItems = aClassifier.classify(items);
    NewsItem classifiedItem = classifiedItems.get(0);
    
    EntityTransaction txn = getEntityManager().getTransaction();
    
    txn.begin();
    em.merge(classifiedItem.getDao());
    txn.commit();    
  }
//	@Test
//	public void testClassify_France24Articles ( )
//	{
//	  this.testClassifyItems("feed_id =243");
//	}
//
//	@Test
//	public void testClassify_KuwaitTimesArticles ( )
//	{
//	  this.testClassifyItems("feed_id =244");
//	}
//
//	@Test
//	public void testClassify_XINHUANEWSArticles ( )
//	{
//	  this.testClassifyItems("feed_id =245");
//	}
//
//	@Test
//	public void testClassify_TheIndianExpressArticles ( )
//	{
//	  this.testClassifyItems("feed_id =246");
//	}
//
//	@Test
//	public void testClassify_DailyIndonesiaArticles ( )
//	{
//	  this.testClassifyItems("feed_id =251");
//	}
//
//	@Test
//	public void testClassify_DailySabahArticles ( )
//	{
//	  this.testClassifyItems("feed_id =255");
//	}
//
//	@Test
//	public void testClassify_TheChosunIlboArticles ( )
//	{
//	  this.testClassifyItems("feed_id =256");
//	}
//
//	@Test
//	public void testClassify_ThaiPBSEnglishNewsArticles ( )
//	{
//	  this.testClassifyItems("feed_id =257");
//	}
//
//	@Test
//	public void testClassify_News24SouthAfricaArticles ( )
//	{
//	  this.testClassifyItems("feed_id =261");
//	}
//
//	@Test
//	public void testClassify_GlasgoHeraldArticles ( )
//	{
//	  this.testClassifyItems("feed_id =263");
//	}
//
//	@Test
//	public void testClassify_DundeeCourierArticles ( )
//	{
//	  this.testClassifyItems("feed_id =270");
//	}
//
//	@Test
//	public void testClassify_DailyRecordArticles ( )
//	{
//	  this.testClassifyItems("feed_id =339");
//	}
//
//	@Test
//	public void testParse_Item3235_YemenPeaceTalks ( )
//	{
//	  this.testParseItems("id = 3235");
//	}
//	
//	@Test
//	public void testParse_Item5812_ACMAwardsLasVegas ( )
//	{
//	  this.testParseItems("id = 5812");
//	}

//	@Test
//	public void testParse_Item6193_HoustonFireDepartment ( )
//	{
//	  this.testParseItems("id = 6193");
//	}
//
//	@Test
//	public void testParse_Item5235_IndianSupremeCourtMaggi ( )
//	{
//	  this.testParseItems("id = 5235");
//	}
//	@Test
//	public void testParseItemContent() {
//		fail("Not yet implemented");
//	}
//	@Test
//	public void testClassify_AllPreviouslyClassifiedByPHPAlgorithm ( )
//	{
//	  this.testClassifyItems("item_vlocation='ALGO'");
//	}
//
//	@Test
//	public void testClassify_AllNewlyHarvestedItems ( )
//	{
//	  this.testClassifyItems("item_vlocation=''");
//	}
//
//	@Test
//	public void testParse_Item5235_ForestfiresInUttarakhand ( )
//	{
//	  this.testClassifyItems("id = 13995");
//	}

	@Test
	public void testClassifyItem1( )
	{
		NewsItem testItem = new NewsItem();
		testItem.setTitle("Tim Kaine to hold event in Charlotte’s NoDa on Thursday");
		testItem.setContent("Democratic vice presidential candidate Tim Kaine will campaign in Charlotte Thursday with an event at Heist Brewery in NoDa to encourage early voting. The event is at noon, with doors &hellip; <a href=\"http://www.charlotteobserver.com/news/politics-government/campaign-tracker-blog/article108854217.html#storylink=rss\">Click to Continue &raquo;</a>");
		
		NewsItemClassifier classUnderTest = new NewsItemClassifier();
		NewsItem result = this.classifyItem(classUnderTest, testItem);
		Assert.assertEquals("Incorrect parser location(s)", "Charlotte;NoDa;Charlotte;Heist Brewery;NoDa", result.getParserLocation());
		Assert.assertEquals("Incorrect parser organization(s)", "", result.getParserOrganization());
		Assert.assertEquals("Incorrect parser person(s)", "Tim Kaine;Tim Kaine", result.getParserPerson());
	}

  
	@Test
	public void testClassifyItem2( )
	{
		NewsItem testItem = new NewsItem();
		testItem.setTitle("Dozens of Japan lawmakers visit controversial war shrine");
		testItem.setContent("<p><strong>TOKYO: </strong>Dozens of Japanese lawmakers visited a controversial war shrine Tuesday, in an annual pilgrimage that has angered China and South Korea, who see it as a painful reminder of Tokyo&rsquo;s warring past.</p>"
                          + "<p>A group of 85 politicians arrived at the leafy Yasukuni shrine in downtown Tokyo during a four-day autumn festival. It was not immediately clear if any cabinet ministers were among the group.</p>"
                          + "<p>Led by priests, the lawmakers, clad in dark suits, entered the main shrine building to pray for Japan&rsquo;s war dead as they bowed at the threshold.</p>"
                          + "<p>The visit comes a day after Prime Minister Shinzo Abe&mdash;who has been criticized for what some see as a revisionist take on the country&rsquo;s wartime record&mdash;sent an offering to the shrine, but avoided a visit.</p>"
                          + "<p>The site honors millions of Japanese war dead, but also senior military and political figures convicted of war crimes after World War II.</p>"
                          + "<p>It has for decades been a flashpoint for criticism by countries that suffered from Japan&rsquo;s colonialism and aggression in the first half of the 20th century, including China and the two Koreas.</p>"
                          + "<p>Visits by senior Japanese politicians routinely draw an angry reaction from Beijing and Seoul, and more controversial than the shrine is an accompanying museum that paints Japan as a liberator of Asia and a victim of the war.</p>"
                          + "<p>Abe and other nationalists say Yasukuni is a place to remember fallen soldiers and compare it to Arlington National Cemetery in the United States.</p>"
                          + "<p>&ldquo;Every country pays respects to people who died for his or her country,&rdquo; Hidehisa Otsuji, who headed the group of lawmakers, told reporters Tuesday.</p>"
                          + "<p>On Monday, speaking in Beijing, Chinese foreign ministry spokeswoman Hua Chunying blasted Abe&rsquo;s offering, urging Japan to &ldquo;reflect on its aggressive history and take concrete actions to win back the trust of its Asian neighbors and the international community&rdquo;.</p>"
                          + "<p>Abe visited in December 2013 to mark his first year in power, a pilgrimage that sparked fury in Beijing and Seoul and earned a diplomatic rebuke from close ally the United States, which said it was &ldquo;disappointed&rdquo; by the action.</p>"
                          + "<p>He has since refrained from going, sending ritual offerings instead. AFP</p>"
                          + "<p>AFP/CC</p>"
                          + "<p>The post <a rel=\"nofollow\" href=\"http://www.manilatimes.net/dozens-japan-lawmakers-visit-controversial-war-shrine/291863/\">Dozens of Japan lawmakers visit controversial war shrine</a> appeared first on <a rel=\"nofollow\" href=\"http://www.manilatimes.net/\">The Manila Times Online</a>.</p>");
		
		NewsItemClassifier classUnderTest = new NewsItemClassifier();
		NewsItem result = this.classifyItem(classUnderTest, testItem);
		Assert.assertEquals("Incorrect parser location(s)", "Japan;TOKYO;China;South Korea;Tokyo;Tokyo;Japan;Japan;China;Koreas;Beijing;Seoul;Japan;Asia;United States;Beijing;Japan;Beijing;Seoul;United States;Japan", result.getParserLocation());
		Assert.assertEquals("Incorrect parser organization(s)", "Arlington National Cemetery;Manila Times Online", result.getParserOrganization());
		Assert.assertEquals("Incorrect parser person(s)", "Shinzo Abe;Abe;Hidehisa Otsuji;Hua Chunying;Abe;Abe", result.getParserPerson());
	}

  
	@Test
	public void testClassifyItem3( )
	{
		NewsItem testItem = new NewsItem();
		testItem.setTitle("The Brexit madness");
		testItem.setContent("<div><div>Author:&nbsp;</div><div><div>Linda S. Heard</div></div></div><div><div><div><span>Tue, 2016-10-18</span></div></div></div><div><div>ID:&nbsp;</div><div><div>1476741363920182300</div></div></div><div><div><div><p>Strange how Britain&rsquo;s Prime Minister Theresa May voted for Britain to remain in the EU yet has morphed into one of Brexit&rsquo;s most determined advocates refusing to contemplate a second referendum because the people have spoken, she says.<br>Even stranger is the revelation that the UK&rsquo;s eccentric Foreign Secretary Boris Johnson wrote a column in support of staying-in just days before he joined the Leavers, which was never published.  He no doubt flip-flopped with an eye on Number Ten; she may have been a closet Brexiteer all along.</p></div></div></div><div><div><div><img src=\"http://www.arabnews.com/sites/default/files/18/10/2016//linda-heard.png\" width=\"200\" height=\"200\" alt=\"\" title=\"Linda S. Heard\"></div></div></div><div><div>Main category:&nbsp;</div><div><div><a href=\"http://www.arabnews.com/category/main-category/opinion/columns\">Columns</a></div></div></div>");
		
		NewsItemClassifier classUnderTest = new NewsItemClassifier();
		NewsItem result = this.classifyItem(classUnderTest, testItem);
		Assert.assertEquals("Incorrect parser location(s)", "Britain;Britain;Brexit;UK", result.getParserLocation());
		Assert.assertEquals("Incorrect parser organization(s)", "EU", result.getParserOrganization());
		Assert.assertEquals("Incorrect parser person(s)", "Theresa May;Boris Johnson", result.getParserPerson());
	}

  
	public static EntityManager getEntityManager ( )
	{
		if (em == null)
		{
		  em = PersistenceManager.INSTANCE.getEntityManager();
		}
		
		return (em);
	}

	private static EntityManager em = null;

}
