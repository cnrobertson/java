package com.milapy.classifier;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Test;

import com.milapy.parser.dao.ItemDAO;

public class NewsItemTest {

	@Test
	public void testNewsItem_ItemDAO()
	{
	  
      ItemDAO dao = new ItemDAO();
      dao.setTitle(testTitle);
      dao.setContent(testContent);
      dao.setParserLocation(testParserLocation);
      dao.setParserOrganization(testParserOrganization);
      dao.setParserPerson(testParserPerson);
      dao.setParserDate(testParserDate);
      
      NewsItem classUnderTest = new NewsItem(dao);
      
      Assert.assertEquals("Incorrect title",               testTitle,              classUnderTest.getTitle());
      Assert.assertEquals("Incorrect content",             testContent,            classUnderTest.getContent());
      Assert.assertEquals("Incorrect parser location",     testParserLocation,     classUnderTest.getParserLocation());
      Assert.assertEquals("Incorrect parser organization", testParserOrganization, classUnderTest.getParserOrganization());
      Assert.assertEquals("Incorrect parser person",       testParserPerson,       classUnderTest.getParserPerson());
      Assert.assertEquals("Incorrect parser date",         testParserDate,         classUnderTest.getParserDate());
	}

	@Test
	public void testNewsItem_NewsItem()
	{
	  
      NewsItem item = new NewsItem();
      item.setTitle(testTitle);
      item.setContent(testContent);
      item.setParserLocation(testParserLocation);
      item.setParserOrganization(testParserOrganization);
      item.setParserPerson(testParserPerson);
      item.setParserDate(testParserDate);
      
      NewsItem classUnderTest = new NewsItem(item);
      
      Assert.assertEquals("Incorrect title",               testTitle,              classUnderTest.getTitle());
      Assert.assertEquals("Incorrect content",             testContent,            classUnderTest.getContent());
      Assert.assertEquals("Incorrect parser location",     testParserLocation,     classUnderTest.getParserLocation());
      Assert.assertEquals("Incorrect parser organization", testParserOrganization, classUnderTest.getParserOrganization());
      Assert.assertEquals("Incorrect parser person",       testParserPerson,       classUnderTest.getParserPerson());
      Assert.assertEquals("Incorrect parser date",         testParserDate,         classUnderTest.getParserDate());
	}
	@Test
	public void testSetGetTitle() {
	  NewsItem classUnderTest = new NewsItem();
	  classUnderTest.setTitle(testTitle);
      Assert.assertEquals("Incorrect title", testTitle, classUnderTest.getTitle());
	}

	@Test
	public void testSetGetContent() {
	  NewsItem classUnderTest = new NewsItem();
	  classUnderTest.setContent(testContent);
      Assert.assertEquals("Incorrect content", testContent, classUnderTest.getContent());
	}

	@Test
	public void testSetGetParserLocation() {
	  NewsItem classUnderTest = new NewsItem();
	  classUnderTest.setParserLocation(testParserLocation);
      Assert.assertEquals("Incorrect parser location", testParserLocation, classUnderTest.getParserLocation());
	}

	@Test
	public void testSetGetParserOrganization() {
	  NewsItem classUnderTest = new NewsItem();
	  classUnderTest.setParserOrganization(testParserOrganization);
      Assert.assertEquals("Incorrect parser organization", testParserOrganization, classUnderTest.getParserOrganization());
	}

	@Test
	public void testSetGetParserPerson() {
	  NewsItem classUnderTest = new NewsItem();
	  classUnderTest.setParserPerson(testParserPerson);
      Assert.assertEquals("Incorrect parser person", testParserPerson, classUnderTest.getParserPerson());
	}

	@Test
	public void testSetGetParserDate() {
	  NewsItem classUnderTest = new NewsItem();
	  classUnderTest.setParserDate(testParserDate);
      Assert.assertEquals("Incorrect parser date", testParserDate, classUnderTest.getParserDate());
	}

  private String testTitle              = "TEST_TITLE";
  private String testContent            = "TEST_CONTENT";
  private String testParserLocation     = "TEST_PARSER_LOCATION";
  private String testParserOrganization = "TEST_PARSER_ORGANIZATION";
  private String testParserPerson       = "TEST_PARSER_PERSON";
  private String testParserDate         = "TEST_PARSER_DATE";
}
