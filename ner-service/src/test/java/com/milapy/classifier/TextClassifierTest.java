package com.milapy.classifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.junit.Assert;
import org.junit.Test;


public class TextClassifierTest {


	public List<NamedEntity> testClassifySentence( String aSentence ) 
	{
	    TextClassifier classUnderTest = null;
		try {
			classUnderTest = new TextClassifier();
		} catch (ClassCastException | ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	    List<NamedEntity> result = classUnderTest.classifySentence(aSentence);
	    
	    return (result);
	}

	public List<NamedEntity> testClassifyArticle( String anArticle )
	{
	  List<NamedEntity> result = new ArrayList<NamedEntity>();
      StringTokenizer tokenizer = new StringTokenizer(anArticle, ".");
      
      List<String> sentences = new ArrayList<String>();
      while (tokenizer.hasMoreTokens())
      {
    	  sentences.add(tokenizer.nextToken() + ".");
      }
		    
	  for ( String sentence : sentences)
	  {
        List<NamedEntity> names = this.testClassifySentence(sentence);
        
        for (NamedEntity name : names)
        {
        	result.add(name);
        }
	  }
	  
	  return (result);
	}

	public void testClassifyText ( String       aText,
			                       int          anExpectedNumberOfNames,
			                       List<String> anExpectedNameList )
	{
	    List<NamedEntity> result = this.testClassifyArticle(aText);
	    
	    result.forEach(n->System.out.println(n));
	    Assert.assertEquals("Incorrect number of names returned", anExpectedNumberOfNames, result.size());
	    
	    int index = 0;
	    for ( NamedEntity name : result)
	    {
	      String expectedName = anExpectedNameList.get(index);
	      
	      if (expectedName != null)
	      {
	        this.assertNamesEqual(expectedName, name);
	      }
	      index++;
	    }
	}

//	@Test
//	public void testClassifyCharlotte()
//	{
//		List<String> expectedNames = new ArrayList<String>();
//		expectedNames.add("Charlotte");
//	    String testText = "This test was written in Charlotte.";
//	    int expectedNumberOfNames = 1;
//	    this.testClassifyText(testText, expectedNumberOfNames, expectedNames);
//	}
//
//	@Test
//	public void testClassifyGlasgow()
//	{
//		List<String> expectedNames = new ArrayList<String>();
//	    int expectedNumberOfNames = 2;
//		expectedNames.add("Glasgow");
//		expectedNames.add("Barras");
//	    String testText = "Glasgow's notorious Barras earmarked as arts events and music quarter in 30m plans.";
//	    this.testClassifyText(testText, expectedNumberOfNames, expectedNames);
//	}
//
//	@Test
//	public void testClassifyGlasgowCommonWealthGames()
//	{
//		List<String> expectedNames = new ArrayList<String>();
//	    int expectedNumberOfNames = 1;
//		expectedNames.add("Scotland");
//		String testText = "Groundbreaking Glasgow 2014 legacy jobs scheme to be targeted in council cuts. A PIONEERING jobs scheme set up as part of the Commonwealth Games legacy will be a casualty as Scotland's largest council prepares to announce over Â£130million of cuts.";
//	    this.testClassifyText(testText, expectedNumberOfNames, expectedNames);
//	}
//
//	@Test
//	public void testClassifyFukushimaTitle()
//	{
//		List<String> expectedNames = new ArrayList<String>();
//	    int expectedNumberOfNames = 0;
//		//expectedNames.add("Fukushima");
//		String testText = "10000 Fukushima children still live outside prefecture after disaster survey shows.";
//	    this.testClassifyText(testText, expectedNumberOfNames, expectedNames);
//	}
//
//	@Test
//	public void testClassifyFukushimaContent1()
//	{
//		List<String> expectedNames = new ArrayList<String>();
//	    int expectedNumberOfNames = 0;
//		//expectedNames.add("Fukushima Prefecture");
//		String testText = "Some 10,000 children whose families fled Fukushima Prefecture due to the March 2011 nuclear disaster have yet to return, prefectural government officials said Thursday.";
//	    this.testClassifyText(testText, expectedNumberOfNames, expectedNames);
//	}
//
//	@Test
//	public void testClassifyFukushimaContent2()
//	{
//		List<String> expectedNames = new ArrayList<String>();
//	    int expectedNumberOfNames = 0;
//		//expectedNames.add("Fukushima No");
//		String testText = "Five years after the earthquake and ensuing tsunami triggered the radiation crisis at the Fukushima No. 1 nuclear complex, families with children continue to have serious reservations about environmental safety.";
//	    this.testClassifyText(testText, expectedNumberOfNames, expectedNames);
//	}
//
//	@Test
//	public void testClassifyNarisawaTokyoTitle()
//	{
//		List<String> expectedNames = new ArrayList<String>();
//	    int expectedNumberOfNames = 2;
//		expectedNames.add("Narisawa");
//		expectedNames.add("Tokyo");
//		String testText = " Narisawa in Tokyo retains second spot in Asian restaurant rankings.";
//	    this.testClassifyText(testText, expectedNumberOfNames, expectedNames);
//	}
//
//	@Test
//	public void testClassifyNarisawaTokyoContent1()
//	{
//		List<String> expectedNames = new ArrayList<String>();
//	    int expectedNumberOfNames = 3;
//		expectedNames.add("Narisawa");
//		expectedNames.add("Tokyo");
//		expectedNames.add("Asia");
//		String testText = "French restaurant Narisawa in Tokyo retained the second spot on the list of the top 50 restaurants in Asia for the third consecutive year.";
//	    this.testClassifyText(testText, expectedNumberOfNames, expectedNames);
//	}
//
//	@Test
//	public void testClassifyNarisawaTokyoContent2()
//	{
//		List<String> expectedNames = new ArrayList<String>();
//	    int expectedNumberOfNames = 2;
//		expectedNames.add("William Reed Business Media Ltd.");
//		expectedNames.add("Bangkok");
//		String testText = "William Reed Business Media Ltd., the organizer of the ranking, announced this year&#8217;s list Monday in Bangkok.";
//	    this.testClassifyText(testText, expectedNumberOfNames, expectedNames);
//	}
//
	@Test
	public void testClassifyNarisawaTokyoContent3()
	{
		List<String> expectedNames = new ArrayList<String>();
	    int expectedNumberOfNames = 2;
		expectedNames.add("Gaggan");
		expectedNames.add("Bangkok");
		String testText = "Indian restaurant Gaggan in Bangkok remained at the top for the second year in a row.";
	    this.testClassifyText(testText, expectedNumberOfNames, expectedNames);
	}

	private void assertNamesEqual( String      anExpectedName,
			                       NamedEntity aName )
	{
	  Assert.assertEquals("Incorrect name returned", anExpectedName, aName.getName());
	}
}
