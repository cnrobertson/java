package com.milapy.classifier;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NamedEntity {

	public static final String PERSON = "PERSON";
	public static final String ORGANIZATION = "ORGANIZATION";
	public static final String LOCATION = "LOCATION";



  public NamedEntity ( List<String> aListOfNameElements,
		               String       aType)
  {
	this.elements = new ArrayList<String>(aListOfNameElements);
	this.type = aType;
  }

  public List<String> getElements ()
  {
	  return (elements);
  }
  
  public String getName ( )
  {
    String result = String.join(" ", this.elements);
    return (result);
  }
  
  public String getType ( )
  {
	  return (this.type);
  }
  
  public String toString ( )
  {
    String result = "(" + this.getName() + ", " + this.getType() + ")";
    return (result);
  }

  private List<String> elements;
  private String       type;
}
