package com.milapy.classifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class NewsItemClassifier {


	public NewsItemClassifier ( )
	{
		try {
			classifier = new TextClassifier();
		} catch (ClassCastException | ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public List<NewsItem> classify ( List<NewsItem> items )
	{
		List<NewsItem> result = new ArrayList<NewsItem>();

		items.stream().forEach(i->result.add(this.classifyItemContent(i)));;

		return (result);
	}
	
//	public NewsItem classifyItemContent ( NewsItem anItem )
//	{
//	  NewsItem result = new NewsItem(anItem);
//	  
//	  String itemText = anItem.getTitle();
//	  
//	  String itemContentText = anItem.getContent();
//	  
//	  if (itemContentText.startsWith("<"))
//	  {
//        Document doc = Jsoup.parseBodyFragment(anItem.getContent());
//	  
//        itemContentText = doc.body().text();
//	  }
//	  
//	  //System.out.println(itemText);
//	  //System.out.println(itemContentText);
//	  if (!itemText.endsWith("."))
//	  {
//		  itemText += ".";
//	  }
//	  itemText += itemContentText;
//	  List<NamedEntity> names = classifier.classifyParagraph(itemText);
//	  //names.forEach(n->System.out.println(n));
//	  
//	  Stream<NamedEntity> locationStream = names.stream().filter(n->n.getType().equals(NamedEntity.LOCATION));
//	  //locationStream.forEach(l->System.out.println(l));
//	  Stream<NamedEntity> locations     = names.stream().filter(n->n.getType().equals(NamedEntity.LOCATION));
//	  List<String> locationNames = locations.map(e->e.getName()).collect(Collectors.toList());
//	  Stream<NamedEntity> organizations = names.stream().filter(n->n.getType().equals(NamedEntity.ORGANIZATION));
//	  List<String> organizationNames = organizations.map(e->e.getName()).collect(Collectors.toList());
//	  Stream<NamedEntity> persons       = names.stream().filter(n->n.getType().equals(NamedEntity.PERSON));
//	  List<String> personNames = persons.map(e->e.getName()).collect(Collectors.toList());
//
//	  boolean misMatch = false;
//	  String algoLocations           = anItem.getParserLocation();
//	  String classifierLocations     = String.join(";", locationNames);
//	  if (!MatchesOrExceeds(algoLocations, locationNames))
//	  {
//		  System.out.println("Location mismatch :");
//		  System.out.println("ALGO : " + algoLocations);
//		  System.out.println("NER  : " + classifierLocations);
//		  misMatch = true;
//	  }
////	  String algoOrganizations       = anItem.getParserLocation();
////	  String classifierOrganizations = String.join(";", organizationNames);
////	  if (!algoOrganizations.equals(classifierOrganizations))
////	  {
////		  System.out.println("Organization mismatch :");
////		  System.out.println("ALGO : " + algoOrganizations);
////		  System.out.println("NER  : " + classifierOrganizations);
////		  misMatch = true;
////	  }
//	  String algoPersons             = anItem.getParserPerson();
//	  String classifierPersons       = String.join(";", personNames);
//	  if (!MatchesOrExceeds(algoPersons, personNames))
//	  {
//		  System.out.println("Person mismatch :");
//		  System.out.println("ALGO : " + algoPersons);
//		  System.out.println("NER  : " + classifierPersons);
//	  }
//	  
//	  if (misMatch)
//	  {
//		  System.out.println("Item " + anItem.getId() + " mis-match (" + ++mismatchCount + " mis-matched items");
//	  }
//	  
//	  if ((++classifiedCount % 100) == 0)
//	  {
//		  System.out.println(classifiedCount + " items classified");
//	  }
//	  
//	  return (result);
//	}
//	
	public NewsItem classifyItemContent ( NewsItem anItem )
	{
	  NewsItem result = new NewsItem(anItem);
	  
	  String itemText = anItem.getTitle();
	  
	  String itemContentText = anItem.getContent();
	  
	  if (itemContentText.startsWith("<"))
	  {
        Document doc = Jsoup.parseBodyFragment(anItem.getContent());
	  
        itemContentText = doc.body().text();
	  }
	  
	  //System.out.println(itemText);
	  //System.out.println(itemContentText);
	  if (!itemText.endsWith("."))
	  {
		  itemText += ".";
	  }
	  itemText += itemContentText;
	  List<NamedEntity> names = classifier.classifyParagraph(itemText);
	  //names.forEach(n->System.out.println(n));
	  
	  Stream<NamedEntity> locationStream = names.stream().filter(n->n.getType().equals(NamedEntity.LOCATION));
	  //locationStream.forEach(l->System.out.println(l));
	  Stream<NamedEntity> locations     = names.stream().filter(n->n.getType().equals(NamedEntity.LOCATION));
	  List<String> locationNames = locations.map(e->e.getName()).collect(Collectors.toList());
	  Stream<NamedEntity> organizations = names.stream().filter(n->n.getType().equals(NamedEntity.ORGANIZATION));
	  List<String> organizationNames = organizations.map(e->e.getName()).collect(Collectors.toList());
	  Stream<NamedEntity> persons       = names.stream().filter(n->n.getType().equals(NamedEntity.PERSON));
	  List<String> personNames = persons.map(e->e.getName()).collect(Collectors.toList());
	  
	  result.setParserLocation(String.join(";", locationNames));
	  result.setParserOrganization(String.join(";", organizationNames));
	  result.setParserPerson(String.join(";", personNames));

      boolean convertLocationToLowerCase = false;

//	  if (result.getTitle().contains("Missing:"))
//	  {
//        System.out.println("missing children item found -->" + result.getTitle() + "<--");
//	  }

	  if (result.getTitle().startsWith("Missing:"))
	  {
        convertLocationToLowerCase = true;
	  }
	  else if (result.getTitle().startsWith("Endangered Missing:"))
	  {
        convertLocationToLowerCase = true;
      }

	  if (convertLocationToLowerCase)
	  {
        String currentLocation = result.getParserLocation();
        result.setParserLocation(currentLocation.toLowerCase());
        if ((++missingPersonLocationCount % 10) == 0)
  	    {
		  System.out.println(missingPersonLocationCount + " missing children locations altered");
	    }
        
	  }
	  result.setvLocation("ALGO_JAVA");
	  
	  if ((++classifiedCount % 100) == 0)
	  {
		  System.out.println(classifiedCount + " items classified");
	  }
	  
	  return (result);
	}
	
	private static boolean MatchesOrExceeds( String       anAlgoString,
			                                 List<String> classifierNames )
	{
	  String[] algoNameStrings = anAlgoString.split(";");
	  
	  boolean result = true;
	  
	  for (String algoName : algoNameStrings)
	  {
		if (algoName.length() > 0)
		{
	      if (!classifierNames.contains(algoName))
	      {
	        String allNames = String.join(" ", classifierNames);
	        if (!allNames.contains(algoName))
	        {
	          result = false;
	        }
	      }
		}
	  }
	  
	  return (result);
	}
	
	private TextClassifier classifier;
	private static int mismatchCount = 0;
	private static int classifiedCount = 0;
	private static int missingPersonLocationCount = 0;

}
