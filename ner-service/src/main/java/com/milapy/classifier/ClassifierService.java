package com.milapy.classifier;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;
import com.milapy.parser.dao.ItemDAO;

@Path("/items/recognize")
@Produces(MediaType.APPLICATION_JSON)
public class ClassifierService {
	public enum ClassifierState {RUNNING, IDLE};
	private final int maxLength;
    private final AtomicLong counter;
    //SLF4J is provided with dropwizard
    Logger log = LoggerFactory.getLogger(ClassifierService.class);

    public ClassifierService(int maxLength) {
        this.maxLength = maxLength;
        this.counter = new AtomicLong();

      this.setClassifierState(ClassifierState.IDLE);
      EntityManagerFactory emf = Persistence.createEntityManagerFactory("milapy");
  	  em = emf.createEntityManager();
  }

    @GET
    @Timed
    public List<Task> classifyItems() {
        List<Task> tasks = new ArrayList<Task>();

        try {
        	  //this.classifyItems("item_vlocation=''");
          	  this.classifyItems("item_vlocation is null AND to_timestamp(item_published) > CURRENT_DATE - 3");
      } catch (Exception e) {
            log.error("Exception in listTasks method.", e);
        }
        return tasks;
    }
    
	private void classifyItems( String aWhereClause )
	{
	  long dbStartTime = System.currentTimeMillis();
	  List<ItemDAO> daoItems = em.createQuery("Select t from " + ItemDAO.class.getSimpleName() + " t where " + aWhereClause).getResultList();

	  long dbEndTime = System.currentTimeMillis();
	  System.out.println("Retrieved " + daoItems.size() + " entries in " + (dbEndTime - dbStartTime) + " ms");

	  this.setStartTime(System.currentTimeMillis());
	  this.setClassifierState(ClassifierState.RUNNING);
	  this.setClassifiedCount(0);
	  List<NewsItem> items = new ArrayList<NewsItem>();
	  daoItems.forEach(i->items.add(new NewsItem(i)));
	  NewsItemClassifier classUnderTest = new NewsItemClassifier();
	  items.forEach(i->this.classifyAndUpdateItem(classUnderTest, i));     
	  long endTime = System.currentTimeMillis();
	  System.out.println("Classified " + this.getClassifiedCount() + " items in " + (endTime - this.getStartTime()) + " ms");
	  this.setClassifierState(ClassifierState.IDLE);
	  // TODO
	  // close the database connection
	  //
	  this.closeDatabaseConnection();
	}

    private void classifyAndUpdateItem ( NewsItemClassifier aClassifier,
                                         NewsItem           anItem )
    {
      List<NewsItem> items = new ArrayList<NewsItem>();
      items.add(anItem);
      List<NewsItem> classifiedItems = aClassifier.classify(items);
      NewsItem classifiedItem = classifiedItems.get(0);

      EntityTransaction txn = em.getTransaction();

      txn.begin();
      em.merge(classifiedItem.getDao());
      txn.commit();
      this.incrementClassifiedCount();
      
      if ((this.getClassifiedCount() % 100) == 1)
      {
    	  long currentTime = System.currentTimeMillis();
    	  System.out.println("Classified " + this.getClassifiedCount() + " items in " + (currentTime - this.getStartTime()) + " ms");

      }
    }

    private void closeDatabaseConnection ( )
    {
      em.close();
      em = null;
    }
    
    public ClassifierState getClassifierState ()
    {
      return (this.currentState);
    }
    
    public void setClassifierState ( ClassifierState aCurrentState )
    {
      this.currentState = aCurrentState;
    }
    
    public synchronized long getClassifiedCount ( )
    {
      return (this.classifiedCount);
    }
    
    public synchronized void incrementClassifiedCount ( )
    {
      this.classifiedCount++;
    }
    
    public synchronized void setClassifiedCount ( long aCount )
    {
      this.classifiedCount = aCount;
    }
    
    public synchronized long getStartTime ( )
    {
      return (this.startTime);
    }
    public synchronized void setStartTime ( long aStartTime )
    {
      this.startTime = aStartTime;
    }
    
	private EntityManager em;
	private long startTime;
	private long classifiedCount;
	
	private ClassifierState currentState;
}