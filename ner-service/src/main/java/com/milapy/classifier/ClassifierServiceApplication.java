package com.milapy.classifier;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class ClassifierServiceApplication extends Application<ClassifierServiceConfiguration> {
    public static void main(String[] args) throws Exception {
        new ClassifierServiceApplication().run(args);
    }

    @Override
    public String getName() {
        return "task-list-service";
    }

    @Override
    public void initialize(Bootstrap<ClassifierServiceConfiguration> bootstrap) {
        // nothing to do yet
    }

    @Override
    public void run(ClassifierServiceConfiguration configuration,
                    Environment environment) {
        // register resource now
        final ClassifierService resource = new ClassifierService(
                configuration.getMaxLength()
        );
        environment.jersey().register(resource);
    }

}