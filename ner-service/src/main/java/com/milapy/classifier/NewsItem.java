package com.milapy.classifier;

import com.milapy.parser.dao.ItemDAO;

public class NewsItem {

	public NewsItem ( )
	{
		this.dao = null;
	}
	
	public NewsItem ( ItemDAO item )
	{
		this.dao                = item;
		this.title              = item.getTitle();
	    this.content            = item.getContent();
	    this.parserLocation     = item.getParserLocation();
	    this.parserOrganization = item.getParserOrganization();
	    this.parserPerson       = item.getParserPerson();
	    this.parserDate         = item.getParserDate();
	}

	public NewsItem ( NewsItem anItem )
	{
		this.dao                = anItem.getDao();
		this.title              = anItem.getTitle();
	    this.content            = anItem.getContent();
	    this.parserLocation     = anItem.getParserLocation();
	    this.parserOrganization = anItem.getParserOrganization();
	    this.parserPerson       = anItem.getParserPerson();
	    this.parserDate         = anItem.getParserDate();
	}
	
	ItemDAO getDao() {
	  return (dao);
	}

	public Integer getId ( )
	{
	  return(this.id);
	}
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getParserLocation() {
		return parserLocation;
	}

	public void setParserLocation(String aParserLocation) {
		this.parserLocation = aParserLocation;
		if (dao != null)
		{
		  this.dao.setParserLocation(aParserLocation);
		}
	}

	public String getParserOrganization() {
		return parserOrganization;
	}

	public void setParserOrganization(String aParserOrganization) {
		this.parserOrganization = aParserOrganization;
		if (dao != null)
		{
		  this.dao.setParserOrganization(aParserOrganization);
		}
	}

	public String getParserPerson() {
		return parserPerson;
	}

	public void setParserPerson(String aParserPerson) {
		this.parserPerson = aParserPerson;
		if (dao != null)
		{
		  this.dao.setParserPerson(aParserPerson);
		}
	}

	public String getParserDate() {
		return parserDate;
	}

	public void setParserDate(String parserDate) {
		this.parserDate = parserDate;
	}
	
	public String getVLocation ( )
	{
return (this.vLocation);
	}
	public void setvLocation ( String aVLocation ) {
	  this.vLocation = aVLocation;
	  if (dao != null)
	  {
	    this.dao.setVLocation(aVLocation);
	  }
	}
	
	private ItemDAO dao;
	private Integer id;
	private String title;
    private String content;
    private String parserLocation;
    private String parserOrganization;
    private String parserPerson;
    private String parserDate;
	private String vLocation;
}
