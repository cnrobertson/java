package com.milapy.classifier;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import edu.stanford.nlp.ie.AbstractSequenceClassifier;
import edu.stanford.nlp.ie.crf.CRFClassifier;
import edu.stanford.nlp.ling.CoreAnnotations.AnswerAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.Tokenizer;
import edu.stanford.nlp.process.TokenizerFactory;


public class TextClassifier {

	public TextClassifier ( ) throws ClassCastException, ClassNotFoundException, IOException
	{
	  String serializedClassifier = "classifiers/english.all.3class.distsim.crf.ser.gz";
	  classifier = CRFClassifier.getClassifier(serializedClassifier);
	}
	
	public List<NamedEntity> classifySentence( String aSentence )
	{
		List<NamedEntity> result = new ArrayList<NamedEntity>();
	    TokenizerFactory<CoreLabel> tokenizerFactory =
	        PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
	    Tokenizer<CoreLabel> tok =
	        tokenizerFactory.getTokenizer(new StringReader(aSentence));
	    List<CoreLabel> rawWords = tok.tokenize();

	    List<List<CoreLabel>> classifierResult = classifier.classify(aSentence);
	    
	    for (List<CoreLabel> element : classifierResult)
	    {
	    	List<String> nameElements = new ArrayList<String>();
	    	String type = "";
	    	boolean nameRecognized = false;
	    	for (CoreLabel label : element)
	    	{
	    		String stanfordType = label.getString(AnswerAnnotation.class);
	    		if (nameRecognized)
	    		// name has already been found - is this another piece of the name
	    		{
	    			if (stanfordType.equals(type))
	    			{
	    				nameElements.add(label.value());
	    			}
	    			else
	    			{
	    				result.add(new NamedEntity(nameElements, type));
	    				nameRecognized = false;
	    			}
	    		}
	    		else if (stanfordType.equals(NamedEntity.LOCATION)
	    		 || stanfordType.equals(NamedEntity.ORGANIZATION)
	    		 || stanfordType.equals(NamedEntity.PERSON))
	    		{
	    			nameRecognized = true;
	    			nameElements.clear();
	    			nameElements.add(label.value());
	    			type = stanfordType;
	    		}
	    	}
	    }
	    return (result);
	}
	
	public List<NamedEntity> classifyParagraph( String aParagraph )
	{
	  List<NamedEntity> result = new ArrayList<NamedEntity>();
      StringTokenizer tokenizer = new StringTokenizer(aParagraph, ".");
      
      List<String> sentences = new ArrayList<String>();
      while (tokenizer.hasMoreTokens())
      {
    	  sentences.add(tokenizer.nextToken() + ".");
      }
		    
	  for ( String sentence : sentences)
	  {
        List<NamedEntity> names = this.classifySentence(sentence);
        
        for (NamedEntity name : names)
        {
        	result.add(name);
        }
	  }
	  
	  return (result);
	}
	
	private AbstractSequenceClassifier<CoreLabel> classifier;
}
