package com.milapy.parser.dao;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name = "fof_item", schema="milapy")
public class ItemDAO {

	public Integer getId() {
		return (this.id);
	}
	public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getContent() {
	return content;
}
public void setContent(String content) {
	this.content = content;
}
public String getParserDate() {
	return parserDate;
}
public void setParserDate (String aDate) {
	this.parserDate = aDate;
}
public String getParserLocation() {
	return parserLocation;
}
public void setParserLocation(String parserLocation) {
	this.parserLocation = parserLocation;
}
public String getParserOrganization() {
	return parserOrganization;
}
public void setParserOrganization(String parserOrganization) {
	this.parserOrganization = parserOrganization;
}
public String getParserPerson() {
	return parserPerson;
}
public void setParserPerson(String parserPerson) {
	this.parserPerson = parserPerson;
}

public String getVLocation ( )
{
  return (vLocation);
}

 public void setVLocation ( String aVLocation )
 {
	 this.vLocation = aVLocation;
 }
  @Id
  @Column(name = "item_id")
  private Integer id;
  @Column(name = "item_title", columnDefinition = "text")
  private String title;
  @Column(name = "item_content", columnDefinition = "text")
  private String content;
  @Column(name = "parser_location", columnDefinition = "text")
  private String parserLocation;
  @Column(name = "parser_organization", columnDefinition = "text")
  private String parserOrganization;
  @Column(name = "parser_person", columnDefinition = "text")
  private String parserPerson;
  @Column(name = "parser_date", columnDefinition = "text")
  private String parserDate;
  @Column(name = "item_vlocation", columnDefinition = "text")
  private String vLocation;
}
